#include <iostream>
#include <memory>
#include <vector>
#include <thread>
#include <string>
#include <sstream>

#include "zmqpp/zmqpp.hpp"

using namespace std::string_literals;

auto server_public_key = R"(umIz8s[!AyQqU$<]9POY4I(YUBxoxv}L#t3>S6K{)";

auto public_key = R"(h$IBr=i)UcVB>7uc3x3&13N18Yvg=ZrXGUmK+w%r)"s;
auto secret_key = R"(q#[j*kWixJdbkAw^B{v@n]A9YBs&YyH?B8p=[rQ9)"s;

int main (int argc, char **argv) {
    std::cout << "Connecting to broker." << std::endl;

    auto ctx = zmqpp::context{};

    auto pub = zmqpp::socket{ ctx, zmqpp::socket_type::pub };
    assert(pub);

    pub.set( zmqpp::socket_option::curve_server_key, server_public_key );
    pub.set( zmqpp::socket_option::curve_public_key, public_key );
    pub.set( zmqpp::socket_option::curve_secret_key, secret_key );

    pub.connect(argc > 1 ? argv[1] : "");

    std::cout << "Connected." << std::endl;

    while (std::cin) {
        auto line = ""s;
        std::getline(std::cin, line);

        std::cout << "Sending: " << line << std::endl;

        auto msg = zmqpp::message{ "test", line };
        pub.send(msg);

        std::cout << "Sent." << std::endl;
    }

    return 0;
}
