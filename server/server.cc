#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include "zmqpp/zmqpp.hpp"

using namespace std::string_literals;

int main (int argc, char **argv) {
    std::cout << "Connecting to broker." << std::endl;

    auto ctx = zmqpp::context{};

    auto sub = zmqpp::socket{ctx, zmqpp::socket_type::subscribe};
    assert(sub);
    sub.connect(argc > 1 ? argv[1] : "tcp://127.0.0.1:9000");
    sub.subscribe("");
    sub.subscribe("test");

    std::cout << "Waiting for messages..." << std::endl;
    while (true) {
        auto msg = zmqpp::message{};
        sub.receive(msg);

        std::cout << "Received message:\n";

        for(auto i = 0u; i < msg.parts(); i++) {
            std::cout << "part " << i << ": " << msg.get(i) << std::endl;
        }
    }

    return 0;
}
