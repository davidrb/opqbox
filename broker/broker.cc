#include "config.hpp"

#include <czmq.h>
#include <iostream>
#include <memory>
#include <vector>
#include <string>

#include "zmqpp/zmqpp.hpp"
#include "zmqpp/proxy.hpp"

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

using namespace std::string_literals;
using std::string;
using std::pair;
using std::cout;
using std::endl;

using boost::filesystem::directory_iterator;

auto load_certificate( string const& path ) -> pair<string, string>;

int main (int argc, char **argv) {
    cout << "Parsing config." << endl;

    auto config = argc == 1 ? Config{} : Config{ argv[1] };

    cout << "public certs: " << config.publicCerts() << '\n';
    cout << "private certs: " << config.privateCert() << '\n';
    cout << "box port: " << config.boxPort() << '\n';
    cout << "backend port: " << config.backendPort() << endl;

    auto server_cert = load_certificate(config.privateCert());

    cout << "server public key " << server_cert.first << endl;

    cout << "Starting broker." << endl;

    auto ctx = zmqpp::context{};

    zmqpp::auth auth{ctx};
    auth.set_verbose(true);
    //auth.configure_curve("CURVE_ALLOW_ANY");

    auto entries = boost::make_iterator_range(directory_iterator(config.publicCerts()), {});
    for(auto& entry : entries) {
        std::cout << entry.path() << "\n";
        auto public_key = load_certificate(entry.path().string()).first;
        auth.configure_curve(public_key);
    }

    auth.configure_domain("*");
//    auth.allow ("127.0.0.1");

    auto front = zmqpp::socket{ ctx, zmqpp::socket_type::xpublish };
    front.bind(config.backendPort());
    assert(front);

    auto back = zmqpp::socket{ ctx, zmqpp::socket_type::xsubscribe };
    back.set( zmqpp::socket_option::identity, "IDENT" );
    back.set( zmqpp::socket_option::curve_server, true );
    back.set( zmqpp::socket_option::curve_secret_key, server_cert.second );
    back.set( zmqpp::socket_option::zap_domain, "global" );
    back.bind(config.boxPort());
    assert(back);

    cout << "Broker started..." << endl;
    zmqpp::proxy{ front, back };

    return 0;
}

#include <regex>
#include <fstream>

using std::regex;
using std::regex_search;

auto public_re = regex{R"r(public-key\s+=\s+"(.+)")r"};
auto private_re = regex{R"r(secret-key\s+=\s+"(.+)")r"};

auto load_certificate( string const& path ) -> pair<string, string> {
    auto file = std::ifstream{ path };
    assert(file);

    auto ss = std::stringstream{};
    ss << file.rdbuf();
    auto contents = ss.str();

    auto public_sm = std::smatch{}, private_sm = std::smatch{};

    auto has_public = regex_search(contents, public_sm, public_re);
    auto has_private = regex_search(contents, private_sm, private_re);

    return {has_public ? public_sm[1] : ""s,
            has_private ? private_sm[1] : ""s};
}
