#include <iostream>

#include "zmqpp/curve.hpp"

using namespace std::string_literals;

int main () {
    auto kp = zmqpp::curve::generate_keypair();
    std::cout << kp.public_key << '\n' << kp.secret_key << std::endl;
    return 0;
}
